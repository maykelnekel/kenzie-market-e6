import { NextFunction, Request, Response } from "express"
import { getRepository } from "typeorm"
import { Order, User } from "../entities"


export const create = async (req: Request, res: Response, next: NextFunction) => {
    const orderRepository = getRepository(Order)
    const userRepository = getRepository(User)
    const { products, userToken } =req.body
  
    try{
        const user = await userRepository.findOne({where: {id: userToken.id}})
        const userOders = user?.orders[user?.orders.length - 1]

        if (userOders?.isBuy || !userOders){
            const newOrder = orderRepository.create({products: products, isBuy: false, user: user})
            await orderRepository.save(newOrder);
            return res.status(201).send(user?.orders);
        } else {
            const currentOrder = await orderRepository.findOne({where:{id: userOders.id}})
            // @ts-ignore
            currentOrder?.products = currentOrder?.products.concat(products)
            // @ts-ignore
            currentOrder?.isBuy = true
            await orderRepository.save(currentOrder as Order)
            const user = await userRepository.findOne({where: {id: userToken.id}})

            return res.status(201).send(user?.orders);
        }

        
    } catch (err) {
        res.status(404).send({mesage: err})
        console.log(err)
    }    
}


export const getOne = async (req: Request, res: Response, next: NextFunction) => {
    const { userToken } =req.body
    const userRepository = getRepository(User)
    try {

        const user = await userRepository.findOne({where: {id: userToken.id}})
        const userOders = user?.orders
        
        res.send({orders: userOders})
    } catch(err)
     {
        console.log(err)
        res.status(404).send({message: err})
     }
}

export const list = async (req: Request, res: Response, next: NextFunction) =>{
    const orderRepository = getRepository(Order)

    const orders = await orderRepository.find()

    res.send({orders:orders})
}