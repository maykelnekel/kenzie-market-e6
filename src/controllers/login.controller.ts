import { NextFunction, Request, Response } from "express";
import bcrypt from 'bcrypt'
import { getRepository } from "typeorm";
import { User } from "../entities";
import jwt from 'jsonwebtoken'

const login = async (req:Request, res: Response, next: NextFunction) =>{
    const {email, password} = req.body

    const userRepository = getRepository(User)
    const user = await userRepository.findOne({where: {email}})

    bcrypt.compare(password, user?.password as string, (err, result) =>{
        if (result){
            const token = jwt.sign(
                { id: user?.id, isAdm: user?.isAdm },
                process.env.SECRET as string,
                {
                  expiresIn: "24h",
                }
              );
              res.status(201).send({token: token})
        }
        else {
            res.status(401).send(err)
        }
    } )
}

export default login