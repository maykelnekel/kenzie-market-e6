import { NextFunction, Request, Response } from "express"
import { getRepository } from "typeorm"
import { Addres, User } from "../entities"


export const list = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getRepository(User)

    const users = await userRepository.find()

    res.send(users)
    
}

export const create = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getRepository(User)
    const addressRepository = getRepository(Addres)
    const {firstName, lastName, password,phone,isAdm ,email, addresses} = req.body;
  
    try{
        const find = await userRepository.findOne({ where: { email: email } });
        if (find) {
            res.status(400).send({ message: "E-mail already registered" });
        } else {

            
            const newAddress = addressRepository.create({ ...addresses });
            await addressRepository.save(newAddress);
            
            // @ts-ignore
            const test = await addressRepository.findOne({where: { id: newAddress.id}})
            console.log(test)
            
            const user = userRepository.create({firstName, lastName, password, phone,isAdm ,email, addresses: [test as Addres]});
            await userRepository.save(user);

        res.status(201).send(user);

        }
    } catch (err) {
        console.log(err);
    }    
}

export const getOne = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getRepository(User)

    const {userId} = req.params

    const user = await userRepository.findOne({id: userId})

    if(user){

        res.send(user)
    }else {
        res.status(400).send({message: "User not founded"})
    }
}
