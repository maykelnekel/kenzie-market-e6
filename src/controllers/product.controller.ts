import { NextFunction, Request, Response } from "express"
import { getRepository } from "typeorm"
import { Product } from "../entities"


export const list = async (req: Request, res: Response, next: NextFunction) => {
    const productRepository = getRepository(Product)

    const products = await productRepository.find()

    res.send(products)
    
}

export const create = async (req: Request, res: Response, next: NextFunction) => {
    const productRepository = getRepository(Product)
    const data = req.body;
  
    try{
        const find = await productRepository.findOne({ where: { name: data.name } });
        if (find) {
            res.status(400).send({ message: "Product already registered" });
        } else {
            const product = productRepository.create({...data});
            await productRepository.save(product);

        res.status(201).send(product);

        }
    } catch (err) {
        console.log(err);
    }    
}

export const getOne = async (req: Request, res: Response, next: NextFunction) => {
    const productRepository = getRepository(Product)

    const {productId} = req.params

    const product = await productRepository.findOne({id: productId})

    if(product){

        res.send(product)
    }else {
        res.status(400).send({message: "Product not founded"})
    }
}
