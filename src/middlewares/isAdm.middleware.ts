import { NextFunction,Request,Response } from "express";


const verifyAdm = (req: Request, res: Response, next: NextFunction) =>{
    const {isAdm} = req.body.userToken
    if (isAdm === false){
        return res.status(401).send({message: "Access denied. Admins only"})
    } else {
        next()
    }
}

export default verifyAdm
