import { NextFunction,Request,Response } from "express";
import { getRepository } from "typeorm";
import { Order, User } from "../entities";


const verifyCurrentCartId = async (req: Request, res: Response, next: NextFunction) =>{
    const {id, isAdm} = req.body.userToken
    const {orderId} = req.params

    const userRepository = getRepository(User)
    const orderRepository = getRepository(Order)
    
    
    const user = await userRepository.findOne({where:{id:id}})
    const order = await orderRepository.findOne({where:{id: orderId}})
    const ordersFilter = user?.orders.filter(item=> item.id === orderId)
    
    if (!order){
        return res.status(404).send({message: "Order not founded"})
    }
    else if (isAdm === false && ordersFilter?.length as number > 0){
        next()
    }else if( isAdm) {
        next()
    } 
    else {
        return res.status(401).send({message: "Permission denied. Only account owner or admin users"})
    }
}

export default verifyCurrentCartId