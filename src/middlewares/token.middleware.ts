import { NextFunction, Response, Request } from "express";
import jwt from "jsonwebtoken";

const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization?.split(" ")[1];

  jwt.verify(token as string, process.env.SECRET as string, (err, decode) => {
    if (err) {
      return next(
        res.status(401).send({ message: "Missing authorization header" })
      );
    } else {
      req.body.userToken = decode;
      next();
    }
  });
};

export default verifyToken