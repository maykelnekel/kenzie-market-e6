import { NextFunction,Request,Response } from "express";


const verifyCurrentId = (req: Request, res: Response, next: NextFunction) =>{
    const {id, isAdm} = req.body.userToken
    const {userId} = req.params
    if (isAdm === false && id === userId){
        next()
    }else if( isAdm) {
        next()
    } 
    else {
        return res.status(401).send({message: "Permission denied. Only account owner or admin users"})
    }
}

export default verifyCurrentId