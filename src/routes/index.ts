import { Express } from "express"
import alterarSenhaRoutes from "./alterar_senha.routes"
import buyRoutes from "./buy.routes"
import cartRoutes from "./cart.routes"
import emailRoutes from "./email.routes"
import loginRoutes from "./login.routes"
import productRoutes from "./product.routes"
import recuperarRoutes from "./recuperar.routes"
import userRoutes from "./user.routes"

const routes = (app: Express) =>{
    buyRoutes(app)
    alterarSenhaRoutes(app)
    cartRoutes(app)
    emailRoutes(app)
    productRoutes(app)
    recuperarRoutes(app)
    userRoutes(app)
    loginRoutes(app)
}

export default routes