import { Express } from "express"
import express from 'express'
import { create, getOne, list } from "../controllers/cart.controller"
import verifyToken from "../middlewares/token.middleware"
import verifyCurrentCartId from "../middlewares/currentCartId.middleware"
import verifyAdm from "../middlewares/isAdm.middleware"

const router = express.Router()

const cartRoutes = (app: Express) => {
    router.post('',verifyToken, create)
    router.get('/', verifyToken, verifyAdm, list)
    router.get('/:orderId', verifyToken, verifyCurrentCartId, getOne)

    app.use('/cart', router)
}

export default cartRoutes