import { Express } from "express"
import express from 'express'
import verifyToken from "../middlewares/token.middleware"
import verifyAdm from "../middlewares/isAdm.middleware"
import { getOne, list } from "../controllers/product.controller"
import { create } from "../controllers/product.controller"

const router = express.Router()

const productRoutes = (app: Express) => {
    router.post('',verifyToken, verifyAdm, create)
    router.get('',verifyToken, list)
    router.get('/:productId',verifyToken, getOne)

    app.use('/product', router)
}

export default productRoutes