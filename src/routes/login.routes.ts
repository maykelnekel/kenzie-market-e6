import { Express } from "express"
import express from 'express'
import login from "../controllers/login.controller"

const router = express.Router()

const loginRoutes = (app: Express) => {
    router.post('', login)

    app.use('/login', router)
}

export default loginRoutes