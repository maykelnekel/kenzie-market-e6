import { Express } from "express"
import express from 'express'
import { list, create, getOne } from "../controllers/user.controller"
import verifyToken from "../middlewares/token.middleware"
import verifyAdm from "../middlewares/isAdm.middleware"
import verifyCurrentId from "../middlewares/currentId.middleware"

const router = express.Router()

const userRoutes = (app: Express) => {
    router.get('',verifyToken, verifyAdm, list)
    router.post('', create)
    router.get('/:userId',verifyToken, verifyCurrentId, getOne)

    app.use('/user', router)
}

export default userRoutes