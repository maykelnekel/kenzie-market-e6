import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import User  from "./users.entity";
import Product from "./products.entity";

@Entity('orders')
class Order{
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    isBuy!: boolean

    @ManyToMany(() => Product, {eager: true})
    @JoinTable()
    products!: Product[]

    @ManyToOne(() => User, user=> user.orders)
    user!: User

}

export default Order