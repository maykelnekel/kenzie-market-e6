import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import  User from "./users.entity";

@Entity('addresses')
class Addres{
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    zipCode!: string

    @Column()
    street!: string

    @Column()
    city!: string

    @Column()
    number!: string

    @Column()
    state!: string
    
    @ManyToOne(() => User, user => user.addresses)
    user!: User

}

export default Addres