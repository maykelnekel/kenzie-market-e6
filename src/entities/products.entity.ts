import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Order } from ".";

@Entity('products')
class Product{
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    name!: string
    
    @Column()
    price!: string

    @ManyToMany(() => Order, orders => orders.products)
    orders!: Order;
}

export default Product