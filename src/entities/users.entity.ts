import {Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinTable, BeforeInsert} from "typeorm";
import Addres from "./adresses.entity";
import Order from "./orders.entity";
import bcrypt from 'bcrypt'


@Entity('users')
class User {

    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    firstName!: string;

    @Column()
    lastName!: string;

    @Column()
    password!: string;

    @Column()
    phone!: string;

    @Column()
    isAdm!: boolean;

    @Column()
    email!: string;

    @OneToMany(() => Addres, address => address.user)
    addresses!: Addres[];

    @OneToMany(()=> Order, order => order.user, {eager: true})
    @JoinTable()
    orders!: Order[]

    @BeforeInsert()
    hashPassword(){
        this.password = bcrypt.hashSync(this.password, 10)
    }

}

export default User